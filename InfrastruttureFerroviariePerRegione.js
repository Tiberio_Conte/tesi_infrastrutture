window.onload = function() {

    Plotly.d3.csv("Dataset/datiCSV/Tav_7.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.09,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x4"
                },
                {
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x5"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 85, l: 105, r: 10},
                font: {
                    size: 12
                },
                xaxis: {
                    domain: [0.02, 0.18],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km",
                    dtick: 250
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis5: {
                    domain: [0.82, 0.98],
                    title: {
                        text: "Totale rete<br>ferroviaira",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km",
                     dtick: 750
                },
                annotations:titoloY
            };
            Plotly.plot('pos1', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Valori assoluti",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleDop,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleSem,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonDop,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonSem,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },{
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: tot,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x5",
                    yaxis: "y5",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 70, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " km",
                },
                yaxis2: {
                    position:0.24,
                    ticksuffix: " km"
                },
                yaxis3: {
                    position:0.44,
                    ticksuffix: " km"
                },
                yaxis4: {
                    position:0.64,
                    ticksuffix: " km"
                },
                yaxis5: {
                    position:0.84,
                    ticksuffix: " km"
                },
                xaxis: {
                    domain: [0.05, 0.15],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.25, 0.35],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.45, 0.55],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.65, 0.75],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis5: {
                    domain: [0.85, 0.95],
                    title: {
                        text: "Totale rete<br>ferroviaria",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations:titoloY
            };
            Plotly.plot('pos2', tracce2, layout2,{displayModeBar: false});
        }
    );

    //cloroMap
     fattoreScala = 0.07128;

    mappaCloropetica("pos3","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_7.csv",2,"km",null,null,null,fattoreScala,210);
    mappaCloropetica("pos4","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_7.csv",3,"km",null,null,null,fattoreScala,25);
    mappaCloropetica("pos5","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_7.csv",4,"km",null,null,null,fattoreScala,120);
    mappaCloropetica("pos6","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_7.csv",5,"km",null,null,null,fattoreScala,0);
    mappaCloropetica("pos6b","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_7.csv",6,"km",null,null,null,fattoreScala,300);








    Plotly.d3.csv("Dataset/datiCSV/Tav_9.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];
            let superficeTerritoriale = [];
            let info= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
                superficeTerritoriale.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²');
                info.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²<br>'+data[data.length-i-1].Territorio)
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.09,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                },{
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x5"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 85, l: 105, r: 10},
                font: {
                    size: 12
                },

                xaxis: {
                    domain: [0.02, 0.18],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },

                    ticksuffix: " kmˉ¹",
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                    dtick: 1
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                    dtick: 15
                },
                xaxis5: {
                    domain: [0.82, 0.985],
                    title: {
                        text: "Totale rete<br>ferroviaria",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                    dtick: 25
                },
                annotations:titoloY
            };
            Plotly.plot('pos7', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Rapporti linea-superfice",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleDop,
                    text:info,
                    hoverinfo:"y+text",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleSem,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonDop,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonSem,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: tot,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x5",
                    yaxis: "y5",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 70, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " kmˉ¹",
                },
                yaxis2: {
                    position:0.24,
                    ticksuffix: " kmˉ¹",
                },
                yaxis3: {
                    position:0.44,
                    ticksuffix: " kmˉ¹",
                },
                yaxis4: {
                    position:0.64,
                    ticksuffix: " kmˉ¹",
                },
                yaxis5: {
                    position:0.84,
                    ticksuffix: " kmˉ¹",
                },
                xaxis: {
                    domain: [0.05, 0.15],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.25, 0.35],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.45, 0.55],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.65, 0.75],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis5: {
                    domain: [0.85, 0.95],
                    title: {
                        text: "Totale rete<br>ferroviaria",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations: titoloY
            };
            Plotly.plot('pos8', tracce2, layout2,{displayModeBar: false});
        }
    );


    //cloroMap
    fattoreScala = 0.07128;

    mappaCloropetica("pos9","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_9.csv",3,"kmˉ¹",2,"Sup: ","km²",fattoreScala,210);
    mappaCloropetica("pos10","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_9.csv",4,"kmˉ¹",2,"Sup: ","km²",fattoreScala,25);
    mappaCloropetica("pos11","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_9.csv",5,"kmˉ¹",2,"Sup: ","km²",fattoreScala,120);
    mappaCloropetica("pos12","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_9.csv",6,"kmˉ¹",2,"Sup: ","km²",fattoreScala,0);
    mappaCloropetica("pos12b","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_9.csv",7,"kmˉ¹",2,"Sup: ","km²",fattoreScala,300);





    Plotly.d3.csv("Dataset/datiCSV/Tav_11.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];
            let popolazione = [];
            let info= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
                popolazione.push('Pop: '+data[data.length-i-1].Popolazione+' a');
                info.push('Pop: '+data[data.length-i-1].Popolazione+' a<br>'+data[data.length-i-1].Territorio)
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.09,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                },{
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x5"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 85, l: 105, r: 10},
                font: {
                    size: 12
                },
                xaxis: {
                    domain: [0.02, 0.18],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },

                    ticksuffix: " km/a",
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a",

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a",
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a",
                },
                xaxis5: {
                    domain: [0.82, 0.985],
                    title: {
                        text: "Totale rete<br>ferroviaria",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a",
                },
                annotations:titoloY
            };
            Plotly.plot('pos13', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Rapporti linea-popolazione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleDop,
                    text:info,
                    hoverinfo:"y+text",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: eleSem,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonDop,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: nonSem,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: tot,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x5",
                    yaxis: "y5",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 60, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " km/a",
                },
                yaxis2: {
                    position:0.24,
                    ticksuffix: " km/a",
                },
                yaxis3: {
                    position:0.44,
                    ticksuffix: " km/a",
                },
                yaxis4: {
                    position:0.64,
                    ticksuffix: " km/a",
                },
                yaxis5: {
                    position:0.84,
                    ticksuffix: " km/a",
                },
                xaxis: {
                    domain: [0.05, 0.15],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.25, 0.35],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.45, 0.55],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.65, 0.75],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis5: {
                    domain: [0.85, 0.95],
                    title: {
                        text: "Totale rete<br>ferroviaria",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations: titoloY
            };
            Plotly.plot('pos14', tracce2, layout2,{displayModeBar: false});
        }
    );


    //cloroMap
   fattoreScala = 0.07128;

    mappaCloropetica("pos15","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_11.csv",3,"km/a",2,"Pop: ","a",fattoreScala,210);
    mappaCloropetica("pos16","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_11.csv",4,"km/a",2,"Pop: ","a",fattoreScala,25);
    mappaCloropetica("pos17","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_11.csv",5,"km/a",2,"Pop: ","a",fattoreScala,120);
    mappaCloropetica("pos18","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_11.csv",6,"km/a",2,"Pop: ","a",fattoreScala,0);
    mappaCloropetica("pos18b","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_11.csv",7,"km/a",2,"Pop: ","a",fattoreScala,300);

};

