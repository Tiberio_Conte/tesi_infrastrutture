
window.onload = function() {

    Plotly.d3.csv("Dataset/datiCSV/Tav_1.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.1,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 70, l: 110, r: 10},
                font: {
                    size: 12
                },

                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis4: {
                    domain: [0.77, 0.98],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                annotations:titoloY
            };
            Plotly.plot('pos1', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Valori assoluti",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: autostrade,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionale,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionali,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradale,
                    text:territorio,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 50, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " km",
                },
                yaxis2: {
                    position:0.29,
                    ticksuffix: " km"
                },
                yaxis3: {
                    position:0.54,
                    ticksuffix: " km"
                },
                yaxis4: {
                    position:0.79,
                    ticksuffix: " km"
                },
                xaxis: {
                    domain: [0.05, 0.20],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.30, 0.45],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.55, 0.70],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.80, 0.95],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations:titoloY
            };
            Plotly.plot('pos2', tracce2, layout2,{displayModeBar: false});
        }
        );

        //cloroMap
        fattoreScala = 0.0891;

        mappaCloropetica("pos3","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_1.csv",2,"km",null,null,null,fattoreScala,210);
        mappaCloropetica("pos4","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_1.csv",3,"km",null,null,null,fattoreScala,25);
        mappaCloropetica("pos5","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_1.csv",4,"km",null,null,null,fattoreScala,120);
        mappaCloropetica("pos6","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_1.csv",5,"km",null,null,null,fattoreScala,0);








    Plotly.d3.csv("Dataset/datiCSV/Tav_3.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];
            let superficeTerritoriale = [];
            let info= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
                superficeTerritoriale.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²');
                info.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²<br>'+data[data.length-i-1].Territorio)
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.1,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 70, l: 110, r: 10},
                font: {
                    size: 12
                },

                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                xaxis4: {
                    domain: [0.77, 0.97],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                annotations:titoloY
            };
            Plotly.plot('pos7', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Rapporti starda-superfice",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: autostrade,
                    text:info,
                    hoverinfo:"y+text",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionale,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionali,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradale,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 50, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " kmˉ¹",
                },
                yaxis2: {
                    position:0.29,
                    ticksuffix: " kmˉ¹"
                },
                yaxis3: {
                    position:0.54,
                    ticksuffix: " kmˉ¹"
                },
                yaxis4: {
                    position:0.79,
                    ticksuffix: " kmˉ¹"
                },
                xaxis: {
                    domain: [0.05, 0.20],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.30, 0.45],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.55, 0.70],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.80, 0.95],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations: titoloY
            };
            Plotly.plot('pos8', tracce2, layout2,{displayModeBar: false});
        }
    );

    //cloroMap
    fattoreScala = 0.0891;
    mappaCloropetica("pos9","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_3.csv",3,"kmˉ¹",2,"Sup: ","km²",fattoreScala,210);
    mappaCloropetica("pos10","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_3.csv",4,"kmˉ¹",2,"Sup: ","km²",fattoreScala,25);
    mappaCloropetica("pos11","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_3.csv",5,"kmˉ¹",2,"Sup: ","km²",fattoreScala,120);
    mappaCloropetica("pos12","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_3.csv",6,"kmˉ¹",2,"Sup: ","km²",fattoreScala,0);









    Plotly.d3.csv("Dataset/datiCSV/Tav_5.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];
            let popolazione = [];
            let info= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
                popolazione.push('Pop: '+data[data.length-i-1].Popolazione+' a');
                info.push('Pop: '+data[data.length-i-1].Popolazione+' a<br>'+data[data.length-i-1].Territorio)
            }


            //barre
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.1,
                y: 0.5,
                textangle:-90,
                text:"Regione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 70, l: 110, r: 10},
                font: {
                    size: 12
                },

                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis4: {
                    domain: [0.77, 0.98],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                annotations:titoloY
            };
            Plotly.plot('pos13', tracce1, layout1,{displayModeBar: false});

            //box plot
            titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.045,
                y: 0.5,
                textangle:-90,
                text:"Rapporti starda-popolazione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: autostrade,
                    text:info,
                    hoverinfo:"y+text",
                    xaxis: "x1",
                    yaxis: "y1",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionale,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x2",
                    yaxis: "y2",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionali,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x3",
                    yaxis: "y3",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                },
                {
                    name: "",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradale,
                    text:info,
                    hoverinfo:"text+y",
                    xaxis: "x4",
                    yaxis: "y4",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                }];

            let layout2 = {
                title: {
                    text: "Visualizzazione tramite box plot",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height: 600,
                showlegend: false,
                margin: {t: 100, b: 30, l: 50, r: 20},
                font: {
                    size: 12
                },
                yaxis: {
                    position:0.04,
                    ticksuffix: " km/a",
                },
                yaxis2: {
                    position:0.29,
                    ticksuffix: " km/a"
                },
                yaxis3: {
                    position:0.54,
                    ticksuffix: " km/a"
                },
                yaxis4: {
                    position:0.79,
                    ticksuffix: " km/a"
                },
                xaxis: {
                    domain: [0.05, 0.20],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis2: {
                    domain: [0.30, 0.45],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis3: {
                    domain: [0.55, 0.70],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                },
                xaxis4: {
                    domain: [0.80, 0.95],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                },
                annotations:titoloY
            };
            Plotly.plot('pos14', tracce2, layout2,{displayModeBar: false});
        }
    );

    //cloroMap
    var fattoreScala = 0.0891;
    mappaCloropetica("pos15","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_5.csv",3,"km/a",2,"Pop: ","a",fattoreScala,210);
    mappaCloropetica("pos16","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_5.csv",4,"km/a",2,"Pop: ","a",fattoreScala,25);
    mappaCloropetica("pos17","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_5.csv",5,"km/a",2,"Pop: ","a",fattoreScala,120);
    mappaCloropetica("pos18","Dataset/cartineCSV/Regioni.csv","Dataset/datiCSV/Tav_5.csv",6,"km/a",2,"Pop: ","a",fattoreScala,0);

};


