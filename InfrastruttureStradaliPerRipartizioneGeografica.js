
window.onload = function() {

    Plotly.d3.csv("Dataset/datiCSV/Tav_2.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];
            let da= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    hoverinfo: "x",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                         font: {
                            size: 18,
                         },
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis4: {
                    domain: [0.77, 0.98],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                annotations: da,
            };
            Plotly.plot('pos1', tracce1, layout1,{displayModeBar: false});
        }
        );



    Plotly.d3.csv("Dataset/datiCSV/Tav_1.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorioMezzoggiorno = [];
            let autostradeMezzoggiorno = [];
            let stradeDiInteresseNazionaleMezzoggiorno = [];
            let stradeProvincialiERegionaliMezzoggiorno = [];
            let totaleReteStradaleMezzoggiorno = [];

            let territorioItaliaCentrale = [];
            let autostradeItaliaCentrale = [];
            let stradeDiInteresseNazionaleItaliaCentrale = [];
            let stradeProvincialiERegionaliItaliaCentrale = [];
            let totaleReteStradaleItaliaCentrale = [];

            let territorioItaliaNordOrientale = [];
            let autostradeItaliaNordOrientale = [];
            let stradeDiInteresseNazionaleItaliaNordOrientale = [];
            let stradeProvincialiERegionaliItaliaNordOrientale = [];
            let totaleReteStradaleItaliaNordOrientale = [];

            let territorioItaliaNordOccidentale = [];
            let autostradeItaliaNordOccidentale = [];
            let stradeDiInteresseNazionaleItaliaNordOccidentale = [];
            let stradeProvincialiERegionaliItaliaNordOccidentale = [];
            let totaleReteStradaleItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                    {
                        territorioMezzoggiorno.push(data[i].Territorio);
                        autostradeMezzoggiorno.push(+data[i].Autostrade);
                        stradeDiInteresseNazionaleMezzoggiorno.push(+data[i].StradeDiInteresseNazionale);
                        stradeProvincialiERegionaliMezzoggiorno.push(+data[i].StradeProvincialiERegionali);
                        totaleReteStradaleMezzoggiorno.push(+data[i].TotaleReteStradale);
                    }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                    {
                        territorioItaliaCentrale.push(data[i].Territorio);
                        autostradeItaliaCentrale.push(+data[i].Autostrade);
                        stradeDiInteresseNazionaleItaliaCentrale.push(+data[i].StradeDiInteresseNazionale);
                        stradeProvincialiERegionaliItaliaCentrale.push(+data[i].StradeProvincialiERegionali);
                        totaleReteStradaleItaliaCentrale.push(+data[i].TotaleReteStradale);
                    }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                    {
                        territorioItaliaNordOrientale.push(data[i].Territorio);
                        autostradeItaliaNordOrientale.push(+data[i].Autostrade);
                        stradeDiInteresseNazionaleItaliaNordOrientale.push(+data[i].StradeDiInteresseNazionale);
                        stradeProvincialiERegionaliItaliaNordOrientale.push(+data[i].StradeProvincialiERegionali);
                        totaleReteStradaleItaliaNordOrientale.push(+data[i].TotaleReteStradale);
                    }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                    {
                        territorioItaliaNordOccidentale.push(data[i].Territorio);
                        autostradeItaliaNordOccidentale.push(+data[i].Autostrade);
                        stradeDiInteresseNazionaleItaliaNordOccidentale.push(+data[i].StradeDiInteresseNazionale);
                        stradeProvincialiERegionaliItaliaNordOccidentale.push(+data[i].StradeProvincialiERegionali);
                        totaleReteStradaleItaliaNordOccidentale.push(+data[i].TotaleReteStradale);
                    }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.6,
                y: 0.5,
                textangle:-90,
                text:"Valori assoluti",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: autostradeMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 245,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:90, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations:titoloY
            };
            Plotly.plot('pos2', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos3', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos4', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos5', tracce5, layout5,{displayModeBar: false});
        }
    );

    //cloroMap
    fattoreScala = 0.0892;

    mappaCloropetica("pos6","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_2.csv",1,"km",null,null,null,fattoreScala,210);
    mappaCloropetica("pos7","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_2.csv",2,"km",null,null,null,fattoreScala,25);
    mappaCloropetica("pos8","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_2.csv",3,"km",null,null,null,fattoreScala,120);
    mappaCloropetica("pos9","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_2.csv",4,"km",null,null,null,fattoreScala,0);





    Plotly.d3.csv("Dataset/datiCSV/Tav_4.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];
            let superficeTerritoriale = [];
            let da= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
                superficeTerritoriale.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²');
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+text",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                        font: {
                            size: 18,
                        },
                        x: 0,
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                xaxis4: {
                    domain: [0.77, 0.98],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                annotations: da
            };
            Plotly.plot('pos10', tracce1, layout1,{displayModeBar: false});
        }
    );



    Plotly.d3.csv("Dataset/datiCSV/Tav_3.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let autostradeMezzoggiorno = [];
            let stradeDiInteresseNazionaleMezzoggiorno = [];
            let stradeProvincialiERegionaliMezzoggiorno = [];
            let totaleReteStradaleMezzoggiorno = [];
            let infoMezzoggiorno = [];

            let autostradeItaliaCentrale = [];
            let stradeDiInteresseNazionaleItaliaCentrale = [];
            let stradeProvincialiERegionaliItaliaCentrale = [];
            let totaleReteStradaleItaliaCentrale = [];
            let infoItaliaCentrale = [];

            let autostradeItaliaNordOrientale = [];
            let stradeDiInteresseNazionaleItaliaNordOrientale = [];
            let stradeProvincialiERegionaliItaliaNordOrientale = [];
            let totaleReteStradaleItaliaNordOrientale = [];
            let infoItaliaNordOrientale = [];

            let autostradeItaliaNordOccidentale = [];
            let stradeDiInteresseNazionaleItaliaNordOccidentale = [];
            let stradeProvincialiERegionaliItaliaNordOccidentale = [];
            let totaleReteStradaleItaliaNordOccidentale = [];
            let infoItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                {
                    autostradeMezzoggiorno.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleMezzoggiorno.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliMezzoggiorno.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleMezzoggiorno.push(+data[i].TotaleReteStradale);
                    infoMezzoggiorno.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                {
                    autostradeItaliaCentrale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaCentrale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaCentrale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaCentrale.push(+data[i].TotaleReteStradale);
                    infoItaliaCentrale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                {
                    autostradeItaliaNordOrientale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaNordOrientale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaNordOrientale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaNordOrientale.push(+data[i].TotaleReteStradale);
                    infoItaliaNordOrientale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                {
                    autostradeItaliaNordOccidentale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaNordOccidentale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaNordOccidentale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaNordOccidentale.push(+data[i].TotaleReteStradale);
                    infoItaliaNordOccidentale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.6,
                y: 0.5,
                textangle:-90,
                text:"Rapporti strada-superfice",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: autostradeMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 245,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:90, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations:titoloY
            };
            Plotly.plot('pos11', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos12', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos13', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹"
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos14', tracce5, layout5,{displayModeBar: false});
        }
    );

    //cloroMap
    fattoreScala = 0.0892;

    mappaCloropetica("pos15","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_4.csv",2,"kmˉ¹",1,"Sup: ","km²",fattoreScala,210);
    mappaCloropetica("pos16","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_4.csv",3,"kmˉ¹",1,"Sup: ","km²",fattoreScala,25);
    mappaCloropetica("pos17","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_4.csv",4,"kmˉ¹",1,"Sup: ","km²",fattoreScala,120);
    mappaCloropetica("pos18","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_4.csv",5,"kmˉ¹",1,"Sup: ","km²",fattoreScala,0);









    Plotly.d3.csv("Dataset/datiCSV/Tav_6.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let autostrade = [];
            let stradeDiInteresseNazionale = [];
            let stradeProvincialiERegionali = [];
            let totaleReteStradale = [];
            let popolazione = [];
            let da = [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                autostrade.push(+data[data.length-i-1].Autostrade);
                stradeDiInteresseNazionale.push(+data[data.length-i-1].StradeDiInteresseNazionale);
                stradeProvincialiERegionali.push(+data[data.length-i-1].StradeProvincialiERegionali);
                totaleReteStradale.push(+data[data.length-i-1].TotaleReteStradale);
                popolazione.push('Pop: '+data[data.length-i-1].Popolazione+' a');
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Autostrade",
                    type: "bar",
                    orientation: "h",
                    x: autostrade,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+text",
                    xaxis: "x1"
                },
                {
                    name: "Strade di interesse nazionale",
                    type: "bar",
                    orientation: "h",
                    x: stradeDiInteresseNazionale,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Strade provinciali e regionali",
                    type: "bar",
                    orientation: "h",
                    x: stradeProvincialiERegionali,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Totale rete stradale",
                    type: "bar",
                    orientation: "h",
                    x: totaleReteStradale,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                }];

            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                        font: {
                            size: 18,
                        },
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.23],
                    title: {
                        text: "Autostrade",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis2: {
                    domain: [0.27, 0.48],
                    title: {
                        text: "Strade di interesse nazionale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"

                },
                xaxis3: {
                    domain: [0.52, 0.73],
                    title: {
                        text: "Strade provinciali e regionali",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis4: {
                    domain: [0.77, 0.98],
                    title: {
                        text: "Totale rete stradale",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                annotations: da
            };
            Plotly.plot('pos19', tracce1, layout1,{displayModeBar: false});
        }
    );



    Plotly.d3.csv("Dataset/datiCSV/Tav_5.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let autostradeMezzoggiorno = [];
            let stradeDiInteresseNazionaleMezzoggiorno = [];
            let stradeProvincialiERegionaliMezzoggiorno = [];
            let totaleReteStradaleMezzoggiorno = [];
            let infoMezzoggiorno = [];

            let autostradeItaliaCentrale = [];
            let stradeDiInteresseNazionaleItaliaCentrale = [];
            let stradeProvincialiERegionaliItaliaCentrale = [];
            let totaleReteStradaleItaliaCentrale = [];
            let infoItaliaCentrale = [];

            let autostradeItaliaNordOrientale = [];
            let stradeDiInteresseNazionaleItaliaNordOrientale = [];
            let stradeProvincialiERegionaliItaliaNordOrientale = [];
            let totaleReteStradaleItaliaNordOrientale = [];
            let infoItaliaNordOrientale = [];

            let autostradeItaliaNordOccidentale = [];
            let stradeDiInteresseNazionaleItaliaNordOccidentale = [];
            let stradeProvincialiERegionaliItaliaNordOccidentale = [];
            let totaleReteStradaleItaliaNordOccidentale = [];
            let infoItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                {
                    autostradeMezzoggiorno.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleMezzoggiorno.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliMezzoggiorno.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleMezzoggiorno.push(+data[i].TotaleReteStradale);
                    infoMezzoggiorno.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                {
                    autostradeItaliaCentrale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaCentrale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaCentrale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaCentrale.push(+data[i].TotaleReteStradale);
                    infoItaliaCentrale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                {
                    autostradeItaliaNordOrientale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaNordOrientale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaNordOrientale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaNordOrientale.push(+data[i].TotaleReteStradale);
                    infoItaliaNordOrientale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                {
                    autostradeItaliaNordOccidentale.push(+data[i].Autostrade);
                    stradeDiInteresseNazionaleItaliaNordOccidentale.push(+data[i].StradeDiInteresseNazionale);
                    stradeProvincialiERegionaliItaliaNordOccidentale.push(+data[i].StradeProvincialiERegionali);
                    totaleReteStradaleItaliaNordOccidentale.push(+data[i].TotaleReteStradale);
                    infoItaliaNordOccidentale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.6,
                y: 0.5,
                textangle:-90,
                text:"Rapporti strada-popolazione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: autostradeItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: autostradeMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 245,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:90, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations:titoloY
            };
            Plotly.plot('pos20', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeDiInteresseNazionaleMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos21', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: stradeProvincialiERegionaliMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos22', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totaleReteStradaleMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 215,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a"
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos23', tracce5, layout5,{displayModeBar: false});
        }
    );

    //cloroMap
    fattoreScala = 0.0892;

    mappaCloropetica("pos24","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_6.csv",2,"km/a",1,"Pop: ","a",fattoreScala,210);
    mappaCloropetica("pos25","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_6.csv",3,"km/a",1,"Pop: ","a",fattoreScala,25);
    mappaCloropetica("pos26","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_6.csv",4,"km/a",1,"Pop: ","a",fattoreScala,120);
    mappaCloropetica("pos27","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_6.csv",5,"km/a",1,"Pop: ","a",fattoreScala,0);

};



