window.onload = function() {
    Plotly.d3.csv("Dataset/datiCSV/Tav_8.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];
            let da= [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    hoverinfo: "x",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x4"
                },
                {
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    hoverinfo: "x+y",
                    xaxis: "x5"
                }];



            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                        font: {
                            size: 18,
                        },
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.18],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km",
                    dtick: 1000,
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km"
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km",
                    dtick:1000
                },
                xaxis5: {
                    domain: [0.82, 0.98],
                    title: {
                        text: "Totale rete<br>ferroviaira",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km",
                },
                annotations: da
            };
            Plotly.plot('pos1', tracce1, layout1,{displayModeBar: false});
        }
    );



    Plotly.d3.csv("Dataset/datiCSV/Tav_7.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorioMezzoggiorno = [];
            let eleDopMezzoggiorno = [];
            let eleSemMezzoggiorno = [];
            let nonDopMezzoggiorno = [];
            let nonSemMezzoggiorno = [];
            let totMezzoggiorno = [];

            let territorioItaliaCentrale = [];
            let eleDopItaliaCentrale = [];
            let eleSemItaliaCentrale = [];
            let nonDopItaliaCentrale = [];
            let nonSemItaliaCentrale = [];
            let totItaliaCentrale = [];

            let territorioItaliaNordOrientale = [];
            let eleDopItaliaNordOrientale = [];
            let eleSemItaliaNordOrientale = [];
            let nonDopItaliaNordOrientale = [];
            let nonSemItaliaNordOrientale = [];
            let totItaliaNordOrientale = [];

            let territorioItaliaNordOccidentale = [];
            let eleDopItaliaNordOccidentale = [];
            let eleSemItaliaNordOccidentale = [];
            let nonDopItaliaNordOccidentale = [];
            let nonSemItaliaNordOccidentale = [];
            let totItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                {
                    territorioMezzoggiorno.push(data[i].Territorio);
                    eleDopMezzoggiorno.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemMezzoggiorno.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totMezzoggiorno.push(+data[i].TotaleReteFerroviaria);
                }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                {
                    territorioItaliaCentrale.push(data[i].Territorio);
                    eleDopItaliaCentrale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaCentrale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaCentrale.push(+data[i].TotaleReteFerroviaria);
                }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                {
                    territorioItaliaNordOrientale.push(data[i].Territorio);
                    eleDopItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOrientale.push(+data[i].TotaleReteFerroviaria);
                }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                {
                    territorioItaliaNordOccidentale.push(data[i].Territorio);
                    eleDopItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOccidentale.push(+data[i].TotaleReteFerroviaria);
                }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.74,
                y: 0.5,
                textangle:-90,
                text:"Valori assoluti",
                showarrow:false,
                font: {size: 18 }
            });

            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleDopMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 205,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:85, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations:titoloY
            };
            Plotly.plot('pos2', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleSemMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos3', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonDopMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos4', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonSemMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos5', tracce5, layout5,{displayModeBar: false});

            let tracce5b = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOccidentale,
                    text:territorioItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOrientale,
                    text:territorioItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totItaliaCentrale,
                    text:territorioItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totMezzoggiorno,
                    text:territorioMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5b = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:60, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km"
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(300,20%,70%)','hsl(300,46.66%,50%)', 'hsl(300,73.32%,30%)','hsl(300,100%,10%)']


            };
            Plotly.plot('pos5b', tracce5b, layout5b,{displayModeBar: false});
        }
    );

    //cloroMap
    fattoreScala = 0.07128;

    mappaCloropetica("pos6","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_8.csv",1,"km",null,null,null,fattoreScala,210);
    mappaCloropetica("pos7","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_8.csv",2,"km",null,null,null,fattoreScala,25);
    mappaCloropetica("pos8","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_8.csv",3,"km",null,null,null,fattoreScala,120);
    mappaCloropetica("pos9","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_8.csv",4,"km",null,null,null,fattoreScala,0);
    mappaCloropetica("pos9b","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_8.csv",5,"km",null,null,null,fattoreScala,300);






    Plotly.d3.csv("Dataset/datiCSV/Tav_10.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];
            let superficeTerritoriale = [];
            let da = [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
                superficeTerritoriale.push('Sup: '+data[data.length-i-1].SuperficeTerritoriale+' km²');
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+text",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                },{
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    text: superficeTerritoriale,
                    hoverinfo: "x+y+text",
                    xaxis: "x5"
                }];


            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                        font: {
                            size: 18,
                        },
                        x: 0,
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.19],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹"
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                    dtick: 10

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                    dtick: 0.2
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                },
                xaxis5: {
                    domain: [0.82, 0.98],
                    title: {
                        text: "Totale rete<br>ferroviaira",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " kmˉ¹",
                },
                annotations: da
            };
            Plotly.plot('pos10', tracce1, layout1,{displayModeBar: false});
        }
    );



    Plotly.d3.csv("Dataset/datiCSV/Tav_9.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let eleDopMezzoggiorno = [];
            let eleSemMezzoggiorno = [];
            let nonDopMezzoggiorno = [];
            let nonSemMezzoggiorno = [];
            let totMezzoggiorno = [];
            let infoMezzoggiorno = [];

            let eleDopItaliaCentrale = [];
            let eleSemItaliaCentrale = [];
            let nonDopItaliaCentrale = [];
            let nonSemItaliaCentrale = [];
            let totItaliaCentrale = [];
            let infoItaliaCentrale = [];

            let eleDopItaliaNordOrientale = [];
            let eleSemItaliaNordOrientale = [];
            let nonDopItaliaNordOrientale = [];
            let nonSemItaliaNordOrientale = [];
            let totItaliaNordOrientale = [];
            let infoItaliaNordOrientale = [];

            let eleDopItaliaNordOccidentale = [];
            let eleSemItaliaNordOccidentale = [];
            let nonDopItaliaNordOccidentale = [];
            let nonSemItaliaNordOccidentale = [];
            let totItaliaNordOccidentale = [];
            let infoItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                {
                    eleDopMezzoggiorno.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemMezzoggiorno.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totMezzoggiorno.push(+data[i].TotaleReteFerroviaria);
                    infoMezzoggiorno.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                {
                    eleDopItaliaCentrale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaCentrale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaCentrale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaCentrale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                {
                    eleDopItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOrientale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaNordOrientale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                {
                    eleDopItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOccidentale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaNordOccidentale.push('Sup: '+data[i].SuperficeTerritoriale+' km²<br>'+data[i].Territorio)
                }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.8,
                y: 0.5,
                textangle:-90,
                text:"Rapporti linea-superfice",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleDopMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 205,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:90, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations:titoloY
            };
            Plotly.plot('pos11', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleSemMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹"
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos12', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonDopMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos13', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonSemMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹",
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos14', tracce5, layout5,{displayModeBar: false});

            let tracce5b = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5b = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " kmˉ¹",
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(300,20%,70%)','hsl(300,46.66%,50%)', 'hsl(300,73.32%,30%)','hsl(300,100%,10%)']


            };
            Plotly.plot('pos14b', tracce5b, layout5b,{displayModeBar: false});
        }
    );

    //cloroMap
     fattoreScala = 0.07128;

    mappaCloropetica("pos15","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_10.csv",2,"kmˉ¹",1,"Sup: ","km²",fattoreScala,210);
    mappaCloropetica("pos16","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_10.csv",3,"kmˉ¹",1,"Sup: ","km²",fattoreScala,25);
    mappaCloropetica("pos17","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_10.csv",4,"kmˉ¹",1,"Sup: ","km²",fattoreScala,120);
    mappaCloropetica("pos18","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_10.csv",5,"kmˉ¹",1,"Sup: ","km²",fattoreScala,0);
    mappaCloropetica("pos18b","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_10.csv",6,"kmˉ¹",1,"Sup: ","km²",fattoreScala,300);








    Plotly.d3.csv("Dataset/datiCSV/Tav_12.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let territorio = [];
            let eleDop = [];
            let eleSem = [];
            let nonDop = [];
            let nonSem = [];
            let tot = [];
            let popolazione = [];
            let da = [];

            for (let i = 0; i < data.length; ++i) {
                territorio.push(data[data.length-i-1].Territorio);
                eleDop.push(+data[data.length-i-1].LineeElettrificateABinarioDoppio);
                eleSem.push(+data[data.length-i-1].LineeElettrificateABinarioSemplice);
                nonDop.push(+data[data.length-i-1].LineeNonElettrificateABinarioDoppio);
                nonSem.push(+data[data.length-i-1].LineeNonElettrificateABinarioSemplice);
                tot.push(+data[data.length-i-1].TotaleReteFerroviaria);
                popolazione.push('Pop: '+data[data.length-i-1].Popolazione+' a');
                da.push({
                    x: 0,
                    y: data[data.length-i-1].Territorio,
                    text:" "+data[data.length-i-1].Territorio,
                    xanchor:'left',
                    showarrow:false,
                    font: {color:'white'}
                })
            }


            //barre
            let tracce1 = [
                {
                    name: "Linee elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: eleDop,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+text",
                    xaxis: "x1"
                },
                {
                    name: "Linee elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: eleSem,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x2"
                },
                {
                    name: "Linee non elettrificate a binario doppio",
                    type: "bar",
                    orientation: "h",
                    x: nonDop,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x3"
                },
                {
                    name: "Linee non elettrificate a binario semplice",
                    type: "bar",
                    orientation: "h",
                    x: nonSem,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x4"
                },{
                    name: "Totale rete ferroviaria",
                    type: "bar",
                    orientation: "h",
                    x: tot,
                    y: territorio,
                    text: popolazione,
                    hoverinfo: "x+y+text",
                    xaxis: "x5"
                }];


            let layout1 = {
                title: {
                    text: "Visualizzazione tramite grafico a barre",
                    font: {
                        size: 22,
                    }
                },
                width: 1255,
                height:350,
                showlegend: false,
                margin: {t: 100, b: 70, l: 30, r:10},
                font: {
                    size: 12
                },
                yaxis: {
                    title: {
                        text: "Ripartizione",
                        font: {
                            size: 18,
                        },
                    },
                    showticklabels:false,
                },
                xaxis: {
                    domain: [0.02, 0.19],
                    title: {
                        text: "Linee elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis2: {
                    domain: [0.22, 0.38],
                    title: {
                        text: "Linee elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a",
                    dtick: 0.4

                },
                xaxis3: {
                    domain: [0.42, 0.58],
                    title: {
                        text: "Linee non elettrificate<br>a binario doppio",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis4: {
                    domain: [0.62, 0.78],
                    title: {
                        text: "Linee non elettrificate<br>a binario semplice",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                xaxis5: {
                    domain: [0.82, 0.98],
                    title: {
                        text: "Totale rete<br>ferroviaira",
                        font: {
                            size: 18,
                        },
                    },
                    ticksuffix: " km/a"
                },
                annotations: da
            };
            Plotly.plot('pos19', tracce1, layout1,{displayModeBar: false});
        }
    );



    Plotly.d3.csv("Dataset/datiCSV/Tav_11.csv",
        function (error, data) {
            if (error) {
                alert("Cannot load data: " + error.toString());
                return;
            }

            let eleDopMezzoggiorno = [];
            let eleSemMezzoggiorno = [];
            let nonDopMezzoggiorno = [];
            let nonSemMezzoggiorno = [];
            let totMezzoggiorno = [];
            let infoMezzoggiorno = [];

            let eleDopItaliaCentrale = [];
            let eleSemItaliaCentrale = [];
            let nonDopItaliaCentrale = [];
            let nonSemItaliaCentrale = [];
            let totItaliaCentrale = [];
            let infoItaliaCentrale = [];

            let eleDopItaliaNordOrientale = [];
            let eleSemItaliaNordOrientale = [];
            let nonDopItaliaNordOrientale = [];
            let nonSemItaliaNordOrientale = [];
            let totItaliaNordOrientale = [];
            let infoItaliaNordOrientale = [];

            let eleDopItaliaNordOccidentale = [];
            let eleSemItaliaNordOccidentale = [];
            let nonDopItaliaNordOccidentale = [];
            let nonSemItaliaNordOccidentale = [];
            let totItaliaNordOccidentale = [];
            let infoItaliaNordOccidentale = [];

            for (let i = 0; i < data.length; ++i) {
                if (data[i].Territorio==="Abruzzo"||data[i].Territorio==="Molise"||data[i].Territorio==="Campania"||data[i].Territorio==="Puglia"||data[i].Territorio==="Basilicata"||data[i].Territorio==="Calabria"||data[i].Territorio==="Sicilia"||data[i].Territorio==="Sardegna")
                {
                    eleDopMezzoggiorno.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemMezzoggiorno.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemMezzoggiorno.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totMezzoggiorno.push(+data[i].TotaleReteFerroviaria);
                    infoMezzoggiorno.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Toscana"||data[i].Territorio==="Umbria"||data[i].Territorio==="Marche"||data[i].Territorio==="Lazio")
                {
                    eleDopItaliaCentrale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaCentrale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaCentrale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaCentrale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaCentrale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Trentino-Alto Adige"||data[i].Territorio==="Veneto"||data[i].Territorio==="Friuli Venezia Giulia"||data[i].Territorio==="Emilia Romagna")
                {
                    eleDopItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOrientale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOrientale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOrientale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaNordOrientale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
                if (data[i].Territorio==="Piemonte"||data[i].Territorio==="Valle d'Aosta"||data[i].Territorio==="Lombardia"||data[i].Territorio==="Liguria")
                {
                    eleDopItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioDoppio);
                    eleSemItaliaNordOccidentale.push(+data[i].LineeElettrificateABinarioSemplice);
                    nonDopItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioDoppio);
                    nonSemItaliaNordOccidentale.push(+data[i].LineeNonElettrificateABinarioSemplice);
                    totItaliaNordOccidentale.push(+data[i].TotaleReteFerroviaria);
                    infoItaliaNordOccidentale.push('Pop: '+data[i].Popolazione+' a<br>'+data[i].Territorio)
                }
            }

            //box plot
            let titoloY=[];
            titoloY.push({
                xref:"paper",
                yref:"paper",
                x: -0.8,
                y: 0.5,
                textangle:-90,
                text:"Rapporti linea-popolazione",
                showarrow:false,
                font: {size: 18 }
            });
            let tracce2 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleDopItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleDopMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout2 = {
                width: 205,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:90, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(210,20%,70%)','hsl(210,46.66%,50%)', 'hsl(210,73.32%,30%)','hsl(210,100%,10%)'],
                annotations: titoloY,
            };
            Plotly.plot('pos20', tracce2, layout2,{displayModeBar: false});
            let tracce3 = [
                {
                    name:"OC",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: eleSemItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: eleSemMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout3 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(25,70%,70%)','hsl(25,80%,56.7%)', 'hsl(25,90%,43.4%)','hsl(25,100%,30%)']
            };
            Plotly.plot('pos21', tracce3, layout3,{displayModeBar: false});
            let tracce4 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonDopItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonDopMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout4 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},
                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20]
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(120,20%,70%)','hsl(120,46.66%,50%)', 'hsl(120,73.32%,30%)','hsl(120,100%,10%)']
            };
            Plotly.plot('pos22', tracce4, layout4,{displayModeBar: false});

            let tracce5 = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: nonSemItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: nonSemMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5 = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(0,20%,70%)','hsl(0,46.66%,50%)', 'hsl(0,73.32%,30%)','hsl(0,100%,10%)']
            };
            Plotly.plot('pos23', tracce5, layout5,{displayModeBar: false});

            let tracce5b = [
                {
                    name: "OC",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOccidentale,
                    text:infoItaliaNordOccidentale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x1"
                },
                {
                    name: "OR",
                    type: "box",
                    orientation: "v",
                    y: totItaliaNordOrientale,
                    text:infoItaliaNordOrientale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x2"
                },
                {
                    name: "CE",
                    type: "box",
                    orientation: "v",
                    y: totItaliaCentrale,
                    text:infoItaliaCentrale,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x3"
                },
                {
                    name: "ME",
                    type: "box",
                    orientation: "v",
                    y: totMezzoggiorno,
                    text:infoMezzoggiorno,
                    hoverinfo:"text+y",
                    boxpoints: 'all',
                    jitter: 0,
                    pointpos: 0,
                    xaxis: "x4"
                }];

            let layout5b = {
                width: 180,
                height: 410,
                showlegend: false,
                margin: {t: 20, b:13, l:65, r: 0},

                font: {
                    size: 12
                },
                yaxis: {
                    ticksuffix: " km/a",
                },

                xaxis: {
                    domain: [0.05, 0.20],
                },
                xaxis2: {
                    domain: [0.30, 0.45]
                },
                xaxis3: {
                    domain: [0.55, 0.70]
                },
                xaxis4: {
                    domain: [0.80, 0.95]
                },
                colorway : ['hsl(300,20%,70%)','hsl(300,46.66%,50%)', 'hsl(300,73.32%,30%)','hsl(300,100%,10%)']


            };
            Plotly.plot('pos23b', tracce5b, layout5b,{displayModeBar: false});
        }
    );

    //cloroMap
    var fattoreScala = 0.07128;

    mappaCloropetica("pos24","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_12.csv",2,"km/a",1,"Pop: ","a",fattoreScala,210);
    mappaCloropetica("pos25","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_12.csv",3,"km/a",1,"Pop: ","a",fattoreScala,25);
    mappaCloropetica("pos26","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_12.csv",4,"km/a",1,"Pop: ","a",fattoreScala,120);
    mappaCloropetica("pos27","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_12.csv",5,"km/a",1,"Pop: ","a",fattoreScala,0);
    mappaCloropetica("pos27b","Dataset/cartineCSV/Ripartizione.csv","Dataset/datiCSV/Tav_12.csv",6,"km/a",1,"Pop: ","a",fattoreScala,300);

};



