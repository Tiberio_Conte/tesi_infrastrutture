function mappaCloropetica(Id, percorsoFileCartina, percorsoFileValori, numeroColonnaDelFileValori, unitaMisuraValori, numeroColonnaInfo, prefissoInfo, suffissoInfo, fattoreScala, coloreHsl) {
            //leggo il fileCartina
            $.ajax({
                url: percorsoFileCartina,
                dataType: 'text',
            }).done(successFunction);
            //Se la richiesta AJAX viene effettuata con successo, viene eseguita successFunction.
            function successFunction(data) {
                     var testoHtml = "";
                     var maxX = 0;
                     var maxY = 0;
                    //effettuiamo il parsing dei dati csv usando la libreria dataParse
                    var dataCartinaParse = Papa.parse(data).data;
                     //ricerca altezza e larghezza massimi della cartina
                     for (let i = 1; i < dataCartinaParse.length; ++i) {
                         let punti = dataCartinaParse[i][1].replace("/", ",").split(",");
                        for(let j = 0; j < punti.length; ++j) {
                            if(((j%2)==0) && (+punti[j]>maxX))
                                maxX=+punti[j];
                            if(((j%2)==1) && (+punti[j]>maxY))
                                maxY=+punti[j];
                        }
                    }

                    //memorizzo prima riga dell'html della cartina
                    testoHtml='<svg width="'+(maxX*fattoreScala)+'" height="'+(maxY*fattoreScala*1.15)+'"  viewBox="0 0 '+(maxX)+' '+(maxY*1.15)+'" preserveAspectRatio="none"> ';

                             //leggo il fileValori
                            $.ajax({
                                url: percorsoFileValori,
                                dataType: 'text',
                            }).done(successFunction);
                            //Se la richiesta AJAX viene effettuata con successo, viene eseguita successFunction.
                            function successFunction(data) {
                                    //effettuiamo il parsing dei dati csv usando la libreria dataParse
                                    var dataValoriParse = Papa.parse(data).data;
                                    //trovo valore massimo e minimo
                                     var maxValore = +dataValoriParse[1][numeroColonnaDelFileValori];
                                     var minValore = 0;
                                     //var minValore = +dataValoriParse[1][numeroColonnaDelFileValori];
                                     for (let i = 1; i < dataValoriParse.length; ++i) {
                                            if(+dataValoriParse[i][numeroColonnaDelFileValori]<minValore)
                                                minValore =dataValoriParse[i][numeroColonnaDelFileValori]
                                            if(+dataValoriParse[i][numeroColonnaDelFileValori]>maxValore)
                                                maxValore =dataValoriParse[i][numeroColonnaDelFileValori]
                                     }
                                    //disegno la cartina
                                    for (let i = 1; i < dataValoriParse.length; ++i) {
                                            var saturazione = 10 + (80/(maxValore-minValore))*(dataValoriParse[i][numeroColonnaDelFileValori]-minValore);
                                            var luminosita = 90 - (80/(maxValore-minValore))*(dataValoriParse[i][numeroColonnaDelFileValori]-minValore);
                                            var poligoni = dataCartinaParse[i][1].split("/");
                                            for(let z=0; z < poligoni.length; ++z) {
                                                let name= dataCartinaParse[i][0].replace("\'","");
                                                testoHtml += ' <polygon' +
                                                    ' onmouseover="document.getElementById(\''+Id+name+'\').style.display = \'block\';"' +
                                                    ' onmouseout="document.getElementById(\''+Id+name+'\').style.display = \'none\';" ' +
                                                    ' style="stroke:black;stroke-width:'+((maxX+maxY)/1000)+'px;fill:hsl(' + coloreHsl + ',' + saturazione + '%, ' + luminosita + '%);"' +
                                                    '  points= "' + poligoni[z] + '">' +
                                                    '</polygon>'

                                            }
                                    }
                                    testoHtml +=
                                        '        <defs>' +
                                        '            <linearGradient id="legenda'+coloreHsl+'"  y1="50%" x1="0%" y2="50%" x2="100%">' +
                                        '                <stop offset="0%" style="stop-color:hsl('+ coloreHsl +',10%,90%);stop-opacity:1" />' +
                                        '                <stop offset="100%" style="stop-color:hsl('+ coloreHsl +',90%,10%);stop-opacity:1" />' +
                                        '            </linearGradient>' +
                                        '        </defs>' +
                                        '        <rect x='+(maxX*0.1)+' y='+(maxY*1.05)+' width='+(maxX*.5)+' height='+(maxY*0.05)+' fill="url(#legenda'+coloreHsl+')">' +
                                        '        </rect>' +
                                        '        <text'+' width='+(maxX*.25)+' height='+(maxY*0.05)+' x='+(maxX*0.1)+' y='+(maxY*1.15)+' style="font-size:'+maxY*0.035+'pt;fill:#2d2d2d;">'+minValore+' '+unitaMisuraValori+'</text>' +
                                        '        <text'+' width='+(maxX*.25)+' height='+(maxY*0.05)+' x='+(maxX*0.6)+' y='+(maxY*1.15)+' style="font-size:'+maxY*0.035+'pt;fill:#2d2d2d;">'+maxValore+' '+unitaMisuraValori+'</text>' +
                                        '    </svg>'




                                        for (let i = 1; i < dataValoriParse.length; ++i) {
                                            testoHtml += '<p id="' + Id + dataCartinaParse[i][0].replace("\'","") +
                                                '" style="display: none;position: fixed;background-color: #fffed7;border-style: solid;border-width: 1px;padding:2px;border-radius: 5px;">'+
                                                dataValoriParse[i][numeroColonnaDelFileValori] + ' ' + unitaMisuraValori + '<br>'
                                            if(numeroColonnaInfo!=null) {
                                                testoHtml += prefissoInfo + dataValoriParse[i][numeroColonnaInfo] + ' ' + suffissoInfo + '<br>'
                                            }
                                            testoHtml += dataCartinaParse[i][0] +
                                                '</p>'

                                        }

                                document.getElementById(Id).innerHTML=testoHtml;

                                for (let i = 1; i < dataValoriParse.length; ++i) {
                                    let name= dataCartinaParse[i][0].replace("\'","");
                                    let bsP = document.getElementById(""+Id + name);
                                    let x, y;
                                    window.addEventListener('mousemove', function(event){
                                        x = event.clientX;
                                        y = event.clientY;
                                        if ( typeof x !== 'undefined' ){
                                            bsP.style.left = x + "px";
                                            bsP.style.top = (y+5) + "px";
                                        }
                                    }, false);
                                }
                            }
            }
}
